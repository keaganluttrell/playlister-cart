package com.playlister.cart.controller;

import com.playlister.cart.model.CartList;
import com.playlister.cart.model.Song;
import com.playlister.cart.model.SongNotFoundException;
import com.playlister.cart.service.CartService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CartController.class)
public class CartControllerTest {

    private final String URI = "/api/cart";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CartService cartService;

    List<Song> testSongs;

    @BeforeEach
    void setup() {
        testSongs = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Song s = new Song(1L + i, "name" + i, "artist" + i, "album" + i);
            testSongs.add(s);
        }
    }

    @Test
    void getCartList_NoParams_ReturnsCartList() throws Exception {
        when(cartService.getCartList()).thenReturn(new CartList(testSongs));

        mockMvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cart", hasSize(5)));
    }

    @Test
    void getCartList_NoParams_NoContent() throws Exception {
        when(cartService.getCartList()).thenReturn(null);

        mockMvc.perform(get(URI))
                .andExpect(status().isNoContent());
    }

    @Test
    void tempPost_ReturnsCart() throws Exception {
        when(cartService.getCartList()).thenReturn(new CartList());

        mockMvc.perform(post(URI))
                .andExpect(status().isOk());
    }

    @Test
    void tempPost_ReturnsCart2() throws Exception {
        when(cartService.getCartList()).thenReturn(null);

        mockMvc.perform(post(URI))
                .andExpect(status().isOk());
    }

    @Test
    void deleteSong_SongId_Accepted() throws Exception {
        mockMvc.perform(delete(URI + "/1"))
                .andExpect(status().isAccepted());
        verify(cartService).deleteSong(anyLong());
    }

    @Test
    void deleteSong_SongId_NoContent() throws Exception {
        doThrow(new SongNotFoundException()).when(cartService).deleteSong(anyLong());
        mockMvc.perform(delete(URI + "/1"))
                .andExpect(status().isNoContent());
    }
}
